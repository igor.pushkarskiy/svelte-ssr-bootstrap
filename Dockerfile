FROM node:14.15.4-alpine3.10

WORKDIR /usr/src/app

COPY .bin /usr/src/app/.bin
COPY node_modules /usr/src/app/node_modules
COPY package.json /usr/src/app/package.json
COPY public /usr/src/app/public

ENV PORT 3000

CMD ["yarn", "start:prod"]