import svelte from 'rollup-plugin-svelte';
import resolve from '@rollup/plugin-node-resolve';
import livereload from 'rollup-plugin-livereload';
import { terser } from 'rollup-plugin-terser';
import multiInput from 'rollup-plugin-multi-input';
import del from 'rollup-plugin-delete';
import css from 'rollup-plugin-css-chunks';
import importAssets from 'rollup-plugin-import-assets';
import sveltePreprocess from 'svelte-preprocess';
import typescript from '@rollup/plugin-typescript';
import alias from '@rollup/plugin-alias';
import replace from '@rollup/plugin-replace';
import path from "path";

const production = !process.env.ROLLUP_WATCH;
const publicStaticPath = process.env.PUBLIC_STATIC_PATH || '/build/client/';
const API_HOST = process.env.API_HOST || '/';

const preprocess = sveltePreprocess({
  defaults: {
    style: 'postcss',
    script: 'typescript',
  },
  postcss: true,
});

const clientConfig = {
  input: ['src/pages/*.svelte'],
  output: {
    dir: 'public/build/client',
    format: 'esm',
    sourcemap: false,
  },
  plugins: [
    del({ targets: 'public/build/client/*' }),
    multiInput(),
    replace({
      'process.env.API_HOST': JSON.stringify(API_HOST),
    }),
    alias({
      entries: [{
        find: 'src',
        replacement: path.resolve(__dirname, 'src')
      }],
    }),
    resolve({ browser: true }),
    svelte({
      preprocess,
      immutable: true,
      hydratable: true,
      emitCss: true,
    }),
    css({
      injectImports: true,
      chunkFileNames: '[name]-[hash].css',
      entryFileNames: '[name].css',
      sourcemap: false,
      emitFiles: true,
    }),
    importAssets({
      fileNames: '[hash].[ext]',
      publicPath: publicStaticPath,
      emitAssets: true,
    }),
    typescript(),
    !production &&
    livereload({
      watch: 'public/build/server',
      delay: 750,
    }),
    production && terser(),
  ],
  watch: {
    clearScreen: false,
  },
};

const serverConfig = {
  input: ['src/pages/*.svelte'],
  output: {
    dir: 'public/build/server',
    format: 'cjs',
    exports: 'default',
  },
  plugins: [
    del({ targets: 'public/build/server/*' }),
    multiInput(),
    alias({
      entries: [{
        find: 'src',
        replacement: path.resolve(__dirname, 'src')
      }],
    }),
    resolve(),
    svelte({
      preprocess,
      immutable: true,
      hydratable: true,
      generate: 'ssr',
    }),
    typescript(),
    importAssets({
      fileNames: '[hash].[ext]',
      publicPath: publicStaticPath,
      emitAssets: false,
    }),
    production && terser(),
  ],
};

export default [clientConfig, serverConfig];
