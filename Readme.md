### Svelte SSR bootstrap server

## How to use that?

Default server address `http://localhost:3000`

### Dev mode 
- Install dependencies `yarn ci`
- Run build client and server sources `yarn build:dev` (Will wait changes from client, then will reload page in browser)
- Run SSR server `yarn start:dev` (Will wait changes front client and server, then will reload server)

### Production mode
- Install dependencies `yarn ci`
- Build sources for production enviroment `yarn build:prod`
- Build api source for production enviroment `yarn build:api`
- Run SSR server `yarn start:prod`

### Linting
- `yarn lint` - Run lint for folders `src` and `server`

### Deployment
#### Copy files to production enviroment
- `.bin`
- `public`
- `package.json`
- `node_modules`

### ENV
- ROLLUP_WATCH - for watch update sources
- NODE_ENV - for chouse build / run options
- PORT - for change default port in production
- HOST - for change default host `0.0.0.0` in production
- PUBLIC_DIR - for overide default path for public artifacts
- PUBLIC_STATIC_PATH - path for static assset in browser

### Project structure
```
├── /public                          	            # directory for public atrifacts which will use in browser
│       └── / build                                 # artifacts
│       │       ├── / client                        # client artifacts
│       │       │       └── / pages                 # list entries with needed to use in browser for start application
│       │       │       ├── {page-name}.{ts,css}    # ts and css files
│       │       │       ....                    
│       │       └── / server                        # only server artifacts
│       │               ├── {page-name}.ts          # only ts files
│       │               ....                    
│       └── / css                                   # extra css files (reset.css, fonts.css, ...etc)
│               ├── {file}.css                      
│               ....                                # only css files
│
├── /server                           	            # nodets express server + express router 
│       │
│       ├── index.ts                                # server start file
│       ├── / api                                   # helpers for API call
│       │       ├── index.ts                        # for define API methods
│       ├── / routes                                # helpers for split logic by pages on server
│       │       ├── index.ts                        # for define routes and http methods
│       │       ├── {route-name}.route.ts                      
│       │       ....                                # only ts files
│       └── / template                              # html template in ts for default page / error page and etc
│               ├── index.ts                        # for define routes and http methods
│               ....                                # only ts files
└── /src
        ├── /components             	            # small components for reuse in widgets
        │       ├── {component-name}.svelte        
        │       ....                                # only svelte files and media(Like png, svg and etc)
        ├── /widgets             	            # big components with logic (Like header, footer and etc)
        │       ├── {widget-name}.svelte           
        │       ....                                # only svelte files
        └── /pages               	            # entries for split source code on small chunks
                ├── {page-type}.svelte              # like home-page.svelt, about-page.svelte
                ....                                # only svelte files
```
