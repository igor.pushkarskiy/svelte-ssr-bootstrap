module.exports = {
  parser: '@typescript-eslint/parser',
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking'
  ],
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
    tsconfigRootDir: process.cwd(),
    project: ['./tsconfig.api.json'],
  },
  env: {
    es6: true,
    browser: true
  },
  plugins: ['@typescript-eslint'],
  ignorePatterns: ['node_modules']
}