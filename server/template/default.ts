interface IPageArguments {
  head?: string | undefined
  html?: string | undefined
  entry?: string | undefined
  props?: Record<string, unknown>
}
export function renderPage({ head, html, entry, props }: IPageArguments): string {
  return `
		<!DOCTYPE html>
		<html lang="en">
		<head>
			${head}
			<meta charset="utf-8">
			<link rel="icon" type="image/png" href="/favicon.png" /> 
			<link rel="stylesheet" href="/css/reset.css"/>
			<link rel="stylesheet" href="/build/client/pages/${entry}.css"/>
			<script type="module" src="/build/client/pages/${entry}.js" defer></script>
		</head>
			<div id="${entry}">${html}</div>
			<script type='module' defer>
				import App from '/build/client/pages/${entry}.js';
				
				new App({
					target: document.getElementById('${entry}'),
					hydrate: true,
					props: JSON.parse('${JSON.stringify(props)}')
				});
			</script>
		<body>
		</body>
		</html>
	`
}
