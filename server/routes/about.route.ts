require('svelte/register')

import type { Request, Response } from 'express'
import * as API from '../api'
import AboutPage from '../../public/build/server/pages/about'
import * as TemplateDefault from '../template/default'

const ENTRY = 'about'

// FIXME: check response.error
export function AboutRoute(req: Request, res: Response): void {
  const params = req.params
  const props = API.getAboutPageState(params) // eslint-disable-line
  const page: Record<string, string> = AboutPage.render(props) // eslint-disable-line

  res.status(200).end(
    TemplateDefault.renderPage({
      html: page.html,
      head: page.head,
      entry: ENTRY,
      props
    })
  )
}
