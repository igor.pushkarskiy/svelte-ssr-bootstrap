require('svelte/register')

import type { Request, Response } from 'express'
import * as API from '../api'
import HomePage from '../../public/build/server/pages/home'
import * as TemplateDefault from '../template/default'

const ENTRY = 'home'

// FIXME: check response.error
export function HomeRoute(req: Request, res: Response): void {
  const params = req.params
  const props = API.getHomePageState(params) // eslint-disable-line
  const page: Record<string, string> = HomePage.render(props) // eslint-disable-line

  res.status(200).end(
    TemplateDefault.renderPage({
      html: page.html,
      head: page.head,
      entry: ENTRY,
      props
    })
  )
}
