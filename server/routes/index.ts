import express from 'express'
import { HomeRoute } from './home.route'
import { AboutRoute } from './about.route'

const router = express.Router()

router.get('/', HomeRoute)
router.get('/about', AboutRoute)

export default router
