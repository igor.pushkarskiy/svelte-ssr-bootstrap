import express from 'express'
import cors from 'cors'
import path from 'path'
import sirv from 'sirv'
import morgan from 'morgan'

import routes from './routes'
import { EOL } from 'os'

const isProduction = process.env.NODE_ENV === 'production'
const port = Number(process.env.PORT) || 3000
const host = process.env.HOST || '0.0.0.0'
const PUBLIC_DIR = process.env.PUBLIC_DIR || path.resolve(process.cwd(), './public')

const server = express()

const assets = sirv(PUBLIC_DIR, {
  dev: !isProduction,
  maxAge: 31536000,
  immutable: true
})

server
  .use(express.json({ limit: '1mb' }))
  .use(cors())
  .use(assets)
  .use(morgan('dev'))
  .use('/', routes)
  .listen(port, host, () =>
    console.log(EOL, `Server is ready http://${host}:${port}`, EOL)
  )
